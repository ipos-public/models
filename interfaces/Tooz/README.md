# Tooz Protobuf Definition

This protobuf data model captures all messages that the Tooz-extension of the IPos-FW receives from- and transmits into its environment, excluding messages communicated with the base part of the IPos-FW that it is an extension of.

