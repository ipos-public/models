# VDA5050 AGV state Protobuf Definition

This protobuf data model is a description of the AGV state messages.
For the demonstrator of Ipos-Framework is the following attributes relevant:
timestamp
manufacturer + serialNumber
agvPosition
loads
batteryState
errors